package com.example.mydemos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MyDemosApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyDemosApplication.class, args);
    }

}
