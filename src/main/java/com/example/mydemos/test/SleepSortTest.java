package com.example.mydemos.test;

/**
 * @author : huangjie121015
 * @date : 2021/7/9 10:26
 */
public class SleepSortTest {
    public static void main(String[] args) {
        int[] ints = {3, 5, 9, 4, 6, 7, 2};
        for (int i : ints) {
            Thread thread = new Thread(() -> {
                try {
                    Thread.sleep(i * 10);
                    System.out.println(i);
                } catch (InterruptedException e) {
                    //todo
                }
            });
            thread.start();
        }
    }
}
