package com.example.mydemos.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class HashTest {

    @Test
    public void f1() {
        System.out.println("刘德华".hashCode());
        System.out.println(Integer.toBinaryString("刘德华".hashCode()));
        System.out.println(Integer.toBinaryString("刘德华".hashCode() >>> 16));
        System.out.println(Integer.toBinaryString("刘德华".hashCode() ^ "刘德华".hashCode() >>> 16));
        System.out.println(Integer.toBinaryString("刘德华0".hashCode()));
        System.out.println(Integer.toBinaryString("刘德华0".hashCode() >>> 16));
        System.out.println(Integer.toBinaryString("刘德华0".hashCode() ^ "刘德华0".hashCode() >>> 16));
    }

    @Test
    public void f2() {

        log.info("1433223的hashcode：{}", "1433223".hashCode());

        log.info("郭德纲的hashcode：{}", "郭德纲".hashCode());
        log.info("郭德纲hashcode的二进制：{}", Integer.toBinaryString("郭德纲".hashCode()));
        log.info("郭德纲hashcode>>>16的二进制：{}", Integer.toBinaryString("郭德纲".hashCode() >>> 16));

        log.info("小郭德纲的hashcode：{}", "小郭德纲".hashCode());
        log.info("彭于晏的hashcode：{}", "彭于晏".hashCode());
        log.info("唱跳rap篮球的hashcode：{}", "唱跳rap篮球".hashCode());


        log.info("(array.length-1)&key.hashcode：{}", 15 & "郭德纲".hashCode());
        log.info("(array.length-1)&key.hashcode：{}", 15 & "小岳岳".hashCode());
        log.info("(array.length-1)&key.hashcode：{}", 15 & "李小龙".hashCode());
        log.info("(array.length-1)&key.hashcode：{}", 15 & "蔡徐坤".hashCode());
        log.info("(array.length-1)&key.hashcode：{}", 15 & "唱跳rap篮球".hashCode());

        log.info("(array.length-1)&key.hashcode：{}", 16 & "郭德纲".hashCode());
        log.info("(array.length-1)&key.hashcode：{}", 16 & "小岳岳".hashCode());
        log.info("(array.length-1)&key.hashcode：{}", 16 & "李小龙".hashCode());
        log.info("(array.length-1)&key.hashcode：{}", 16 & "蔡徐坤".hashCode());
        log.info("(array.length-1)&key.hashcode：{}", 16 & "唱跳rap篮球".hashCode());


    }

    @Test
    public void f3() {

        log.info("数组长度不-1：{}", 16 & "郭德纲".hashCode());
        log.info("数组长度不-1：{}", 16 & "彭于晏".hashCode());
        log.info("数组长度不-1：{}", 16 & "李小龙".hashCode());
        log.info("数组长度不-1：{}", 16 & "蔡徐鸡".hashCode());
        log.info("数组长度不-1：{}", 16 & "唱跳rap篮球鸡叫".hashCode());

        log.info("数组长度-1但是不进行异或和>>>16运算：{}", 15 & "郭德纲".hashCode());
        log.info("数组长度-1但是不进行异或和>>>16运算：{}", 15 & "彭于晏".hashCode());
        log.info("数组长度-1但是不进行异或和>>>16运算：{}", 15 & "李小龙".hashCode());
        log.info("数组长度-1但是不进行异或和>>>16运算：{}", 15 & "蔡徐鸡".hashCode());
        log.info("数组长度-1但是不进行异或和>>>16运算：{}", 15 & "唱跳rap篮球鸡叫".hashCode());

        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("郭德纲".hashCode() ^ ("郭德纲".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("彭于晏".hashCode() ^ ("彭于晏".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("李小龙".hashCode() ^ ("李小龙".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("蔡徐鸡".hashCode() ^ ("蔡徐鸡".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("唱跳rap篮球鸡叫".hashCode() ^ ("唱跳rap篮球鸡叫".hashCode() >>> 16)));

    }

    @Test
    public void f4() {
        log.info("16的二进制码：{}", Integer.toBinaryString(15));
        log.info("key的二进制码：{}", Integer.toBinaryString("郭德纲".hashCode()));
        log.info("数组长度不-1：{}", 16 & "郭德纲".hashCode());
        log.info("数组长度不-1：{}", 16 & "郭德纲".hashCode());

        log.info("(length-1)&key的hash值：{}", 14 & "郭德纲".hashCode());
        log.info("key的hash值%length：{}", "郭德纲".hashCode() % 15);

        log.info("唱跳rap篮球鸡叫.hashCode()：{}", Integer.toBinaryString("唱跳rap篮球鸡叫".hashCode()));
        log.info("唱跳rap篮球鸡叫.hashCode()>>>16：{}", Integer.toBinaryString("唱跳rap篮球鸡叫".hashCode() >>> 16));
        log.info("唱跳rap篮球鸡叫.hashCode()并且进行异或和>>>16运算：{}", Integer.toBinaryString("唱跳rap篮球鸡叫".hashCode() ^ ("唱跳rap篮球鸡叫".hashCode() >>> 16)));

        log.info("郭德纲.hashCode()并且进行异或和>>>16运算：{}", Integer.toBinaryString("郭德纲".hashCode() ^ ("郭德纲".hashCode() >>> 16)));
        log.info("彭于晏.hashCode()并且进行异或和>>>16运算：{}", Integer.toBinaryString("彭于晏".hashCode() ^ ("彭于晏".hashCode() >>> 16)));
        log.info("李小龙.hashCode()并且进行异或和>>>16运算：{}", Integer.toBinaryString("李小龙".hashCode() ^ ("李小龙".hashCode() >>> 16)));
        log.info("蔡徐鸡.hashCode()并且进行异或和>>>16运算：{}", Integer.toBinaryString("蔡徐鸡".hashCode() ^ ("蔡徐鸡".hashCode() >>> 16)));
        log.info("迪丽热巴.hashCode()并且进行异或和>>>16运算：{}", Integer.toBinaryString("迪丽热巴".hashCode() ^ ("迪丽热巴".hashCode() >>> 16)));
    }

    @Test
    public void f5() {

        log.info("郭德纲：{}", Integer.toBinaryString("郭德纲".hashCode()));
        log.info("彭于晏：{}", Integer.toBinaryString("彭于晏".hashCode()));
        log.info("李小龙：{}", Integer.toBinaryString("李小龙".hashCode()));
        log.info("蔡徐鸡：{}", Integer.toBinaryString("蔡徐鸡".hashCode()));
        log.info("唱跳rap篮球鸡叫：{}", Integer.toBinaryString("唱跳rap篮球鸡叫".hashCode()));

        log.info("郭德纲>>>16：{}", Integer.toBinaryString("郭德纲".hashCode() >>> 16));
        log.info("彭于晏>>>16：{}", Integer.toBinaryString("彭于晏".hashCode() >>> 16));
        log.info("李小龙>>>16：{}", Integer.toBinaryString("李小龙".hashCode() >>> 16));
        log.info("蔡徐鸡>>>16：{}", Integer.toBinaryString("蔡徐鸡".hashCode() >>> 16));
        log.info("唱跳rap篮球鸡叫>>>16：{}", Integer.toBinaryString("唱跳rap篮球鸡叫".hashCode() >>> 16));

        log.info("郭德纲：{}", Integer.toBinaryString("郭德纲".hashCode() ^ ("郭德纲".hashCode() >>> 16)));
        log.info("彭于晏：{}", Integer.toBinaryString("彭于晏".hashCode() ^ ("彭于晏".hashCode() >>> 16)));
        log.info("李小龙：{}", Integer.toBinaryString("李小龙".hashCode() ^ ("李小龙".hashCode() >>> 16)));
        log.info("蔡徐鸡：{}", Integer.toBinaryString("蔡徐鸡".hashCode() ^ ("蔡徐鸡".hashCode() >>> 16)));
        log.info("唱跳rap篮球鸡叫：{}", Integer.toBinaryString("唱跳rap篮球鸡叫".hashCode() ^ ("唱跳rap篮球鸡叫".hashCode() >>> 16)));

        log.info("郭德纲：{}", 15 & "郭德纲".hashCode());
        log.info("彭于晏：{}", 15 & "彭于晏".hashCode());
        log.info("李小龙：{}", 15 & "李小龙".hashCode());
        log.info("蔡徐鸡：{}", 15 & "蔡徐鸡".hashCode());
        log.info("唱跳rap篮球鸡叫：{}", 15 & "唱跳rap篮球鸡叫".hashCode());

        log.info("郭德纲：{}", 15 & ("郭德纲".hashCode() ^ ("郭德纲".hashCode() >>> 16)));
        log.info("彭于晏：{}", 15 & ("彭于晏".hashCode() ^ ("彭于晏".hashCode() >>> 16)));
        log.info("李小龙：{}", 15 & ("李小龙".hashCode() ^ ("李小龙".hashCode() >>> 16)));
        log.info("蔡徐鸡：{}", 15 & ("蔡徐鸡".hashCode() ^ ("蔡徐鸡".hashCode() >>> 16)));
        log.info("唱跳rap篮球鸡叫：{}", 15 & ("唱跳rap篮球鸡叫".hashCode() ^ ("唱跳rap篮球鸡叫".hashCode() >>> 16)));

    }

    @Test
    public void f6() {

        log.info("郭德纲：{}", Integer.toBinaryString("郭德纲".hashCode()));
        log.info("彭于晏：{}", Integer.toBinaryString("彭于晏".hashCode()));
        log.info("李小龙：{}", Integer.toBinaryString("李小龙".hashCode()));
        log.info("蔡徐鸡：{}", Integer.toBinaryString("蔡徐鸡".hashCode()));
        log.info("唱跳rap篮球鸡叫：{}", Integer.toBinaryString("唱跳rap篮球鸡叫".hashCode()));

        log.info("郭德纲>>>16：{}", Integer.toBinaryString("郭德纲".hashCode() >>> 16));
        log.info("彭于晏>>>16：{}", Integer.toBinaryString("彭于晏".hashCode() >>> 16));
        log.info("李小龙>>>16：{}", Integer.toBinaryString("李小龙".hashCode() >>> 16));
        log.info("蔡徐鸡>>>16：{}", Integer.toBinaryString("蔡徐鸡".hashCode() >>> 16));
        log.info("唱跳rap篮球鸡叫>>>16：{}", Integer.toBinaryString("唱跳rap篮球鸡叫".hashCode() >>> 16));

    }

    @Test
    public void f7() {
        System.out.println("10001011000001111110001000");
        System.out.println("00000000000000001000101100");
        System.out.println("10001011000001110110100100");
        List<Object> list = Collections.synchronizedList(new ArrayList<>());
        log.info("郭德纲：{}", Integer.toBinaryString("郭德纲".hashCode()));
        log.info("郭德纲：{}", Integer.toBinaryString("郭德纲".hashCode() >>> 16));
        log.info("郭德纲：{}", Integer.toBinaryString("郭德纲".hashCode() ^ "郭德纲".hashCode() >>> 16));
        log.info("郭德纲：{}", Integer.toBinaryString("郭德纲".hashCode() & "郭德纲".hashCode() >>> 16));
        log.info("郭德纲：{}", Integer.toBinaryString("郭德纲".hashCode() | "郭德纲".hashCode() >>> 16));
    }

    @Test
    public void f8() {
        log.info(">>>16运算：{}", 15 & ("郭德纲".hashCode() ^ ("郭德纲".hashCode() >>> 16)));
        log.info(">>>16运算：{}", 15 & ("彭于晏".hashCode() ^ ("彭于晏".hashCode() >>> 16)));
        log.info(">>>16运算：{}", 15 & ("李小龙".hashCode() ^ ("李小龙".hashCode() >>> 16)));
        log.info(">>>16运算：{}", 15 & ("蔡徐鸡".hashCode() ^ ("蔡徐鸡".hashCode() >>> 16)));

        log.info(">>>16运算：{}", 15 & ("郭德纲".hashCode() ^ ("郭德纲".hashCode() >>> 8)));
        log.info(">>>16运算：{}", 15 & ("彭于晏".hashCode() ^ ("彭于晏".hashCode() >>> 8)));
        log.info(">>>16运算：{}", 15 & ("李小龙".hashCode() ^ ("李小龙".hashCode() >>> 8)));
        log.info(">>>16运算：{}", 15 & ("蔡徐鸡".hashCode() ^ ("蔡徐鸡".hashCode() >>> 8)));

        log.info(">>>16运算：{}", 15 & ("郭德纲".hashCode() ^ ("郭德纲".hashCode() >>> 12)));
        log.info(">>>16运算：{}", 15 & ("彭于晏".hashCode() ^ ("彭于晏".hashCode() >>> 12)));
        log.info(">>>16运算：{}", 15 & ("李小龙".hashCode() ^ ("李小龙".hashCode() >>> 12)));
        log.info(">>>16运算：{}", 15 & ("蔡徐鸡".hashCode() ^ ("蔡徐鸡".hashCode() >>> 12)));
    }

    @Test
    public void f9() {
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("郭德纲".hashCode() ^ ("郭德纲".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("彭于晏".hashCode() ^ ("彭于晏".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("李小龙".hashCode() ^ ("李小龙".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("蔡徐鸡".hashCode() ^ ("蔡徐鸡".hashCode() >>> 16)));

        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("郭德纲".hashCode() | ("郭德纲".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("彭于晏".hashCode() | ("彭于晏".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("李小龙".hashCode() | ("李小龙".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("蔡徐鸡".hashCode() | ("蔡徐鸡".hashCode() >>> 16)));

        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("郭德纲".hashCode() & ("郭德纲".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("彭于晏".hashCode() & ("彭于晏".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("李小龙".hashCode() & ("李小龙".hashCode() >>> 16)));
        log.info("数组长度-1并且进行异或和>>>16运算：{}", 15 & ("蔡徐鸡".hashCode() & ("蔡徐鸡".hashCode() >>> 16)));
    }
}
