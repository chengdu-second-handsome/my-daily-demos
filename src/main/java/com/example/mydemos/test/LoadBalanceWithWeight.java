package com.example.mydemos.test;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * @author : huangjie121015
 * @date : 2021/7/8 10:38
 */
public class LoadBalanceWithWeight {

    @Test
    public void test() {
        ArrayList<Goods> goods = new ArrayList<>();
        goods.add(new Goods(1, 1));
        goods.add(new Goods(2, 3));
        goods.add(new Goods(3, 6));
        goods.add(new Goods(4, 10));
        goods.add(new Goods(5, 30));
        int totalWeight = goods.stream().mapToInt(good -> good.weight).sum();
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 1; i <= 300; i++) {
            Goods max = goods.get(0);
            for (Goods good : goods) {
                good.currentWeight += good.weight;
                max = max.currentWeight > good.currentWeight ? max : good;
            }
            max.currentWeight -= totalWeight;
            if (Objects.isNull(map.get(max.num))) {
                map.put(max.num, 1);
            } else {
                Integer count = map.get(max.num);
                map.put(max.num, ++count);
            }
            System.out.print(max.num);
        }
        System.out.print("\n" + map);
    }

    private class Goods {
        Integer num;
        Integer weight;
        Integer currentWeight;

        Goods(Integer num, Integer weight) {
            this.num = num;
            this.weight = weight;
            this.currentWeight = 0;
        }
    }
}